from flask import Flask, render_template, request, jsonify
from flask_restful import Resource, Api
from requests import put,get,delete
from firebase_admin import credentials, firestore, initialize_app
from random import choice

api = Flask(__name__)

cred = credentials.Certificate('key.json')
default_app = initialize_app(cred)
db = firestore.client()
todo_ref = db.collection('todos')

@api.route('/')
def index():
	return render_template('index.html')

@api.route('/api/v1')
def doc():
	return render_template('doc.html')

@api.route('/api/v1/topics', methods=['GET', 'POST'])
def topics():
	return render_template('topics.html')
def create():
    try:
        id = request.json['id']
        todo_ref.document(id).set(request.json)
        return jsonify({"success": True}), 200
    except Exception as e:
        return f"An Error Occured in create: {e}"
def read():
	print("coucou")
    # try:
    #     # Check if ID was passed to URL query
    #     todo_id = request.args.get('id')
    #     if todo_id:
    #         todo = todo_ref.document(todo_id).get()
    #         return jsonify(todo.to_dict()), 200
    #     else:
    #         all_todos = [doc.to_dict() for doc in todo_ref.stream()]
    #         return jsonify(all_todos), 200
    # except Exception as e:
    #     return f"An Error Occured in read: {e}"

@api.route('/api/var1/topics/{id}', methods=['GET,PUT,DELETE'])
def update():
    try:
        id = request.json['id']
        todo_ref.document(id).put(request.json)
        return jsonify({"success": True}), 200
    except Exception as e:
        return f"An Error Occured while updating: {e}"
def delete():
    try:
        # Check for ID in URL query
        todo_id = request.args.get('id')
        todo_ref.document(todo_id).delete()
        return jsonify({"success": True}), 200
    except Exception as e:
        return f"An Error Occured while deleting: {e}"

api.run(host='0.0.0.0', port=1234)